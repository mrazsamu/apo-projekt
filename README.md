# APO-Grafo Dokumentace

Zdrojový kód projektu je dostupný na adrese https://gitlab.fel.cvut.cz/mrazsamu/apo-projekt

## Kompilace, instalace a spuštění

Zkompilovat projekt lze jednoduše pomocí přiloženého makefile (```make run```). Tento příkaz rovnou i instaluje aplikaci na desku, která je připojena k PC přes tyto kroky:

1. Připojení na desku: ```tio /dev/ttyUSB0```, login: root, password: mzAPO35
3. Zkopírute IP adresu do makeFile
4. přidejte ssh klíč: ```ssh-add /opt/apo/zynq/ssh-connect/mzapo-root-key```
5. Hotovo!

## Architektura aplikace

Hlavní část aplikace se vyskytuje v souboru main.c, která využívá funkce z **mzapo_parlcd.h** (lcd display), **mzapo_phys.h** (mapování fyzických adres), **mzapo_regs.h** (registry MZ_APO) a **font_types.h** (fonty pro kreslení textu). 

## Dokumentace

Hlavní smyčka aplikace se stará o posun bodu za čas a volá funkce nutné k kreslení outputu na obrazovku. Dále řeší uživatelské inputy (posun, změna barvy, clearování obrazovky). Používá k tomu následující funkce:

- ```getKnobRotation(unsigned char * mem_base, int offset)```: vrací otočení knoflíku podle indexu (viz dokumentace MZ_APO hw). Hodnoty jsou od 0-250.
- ```drawColMenu(...)```: Kreslí spodní menu s výběrem barev.
- ```drawToLed(unsigned char * address, unsigned char * colors, int selectedCol)```: Kreslí vybranou barvu na ledku podle adresy. Přijímá barvy v formátu podle (MZ_APO hw dokumentace).
- ```updateScreen(...)```: Refreshne lcd obrazovku a nakreslí na ni to co je zrovna v kreslícím bufferu.
- ```drawPixelCol(int x, int y, unsigned short col)```: Překreslí zvolenou pozici na zvolenou barvu 
- ```drawStats(...)```: Vypíše stav aplikace na zvolené místo

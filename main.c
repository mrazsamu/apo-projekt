/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON
 
  change_me.c      - main file
 
  include your name there and license for distribution.
 
  Remove next text: This line should not appear in submitted
  work and project name should be change to match real application.
  If this text is there I want 10 points subtracted from final
  evaluation.
 
 *******************************************************************/
 
#define _POSIX_C_SOURCE 200112L

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 480
#define COLOR_MENU_WIDTH 10
 
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
 
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"
 
unsigned short *fb;
font_descriptor_t *fdes;

uint32_t getKnobRotation(unsigned char * mem_base, int offset){
  return (uint32_t)*(mem_base + SPILED_REG_KNOBS_8BIT_o + offset);
}

void drawToLed(unsigned char * address, unsigned char * colors, int selectedCol){
  *address = colors[selectedCol];
  *(address+1) = colors[selectedCol+1];
  *(address+2) = colors[selectedCol+2];
}

uint32_t getKnobPressed(unsigned char * mem_base, char mask){
  char buttons = *(mem_base + SPILED_REG_KNOBS_8BIT_o + 3);
  return (buttons & mask) >> (mask / 2);
}

void drawPixel(unsigned short * fb, int x, int y, unsigned short r, unsigned short g, unsigned short b){
  if (!(x>=0 && x< SCREEN_HEIGHT && y>=0 && y< SCREEN_WIDTH))
    return;
  
  r = r & 0x1f;
  b = b & 0x1f;
  g = g & 0x3f;
  unsigned short col = 0;
  col = col | b;
  col = col | (g << 5);
  col = col | (r << 11);

  fb[x+y*SCREEN_HEIGHT] = col;
}

void drawPixelCol(int x, int y, unsigned short col){
  if (!(x>=0 && x< SCREEN_HEIGHT && y>=0 && y< SCREEN_WIDTH))
    return;

  fb[x+y*SCREEN_HEIGHT] = col;
}

void updateScreen(unsigned char *parlcd_mem_base, unsigned short * fb){
    parlcd_write_cmd(parlcd_mem_base, 0x2c); // write data
    for (int i = 0; i < SCREEN_WIDTH*SCREEN_HEIGHT; i++) {
      parlcd_write_data(parlcd_mem_base, fb[i]);
    }
}

int clamp(int val, int min, int max){
  if(val < min)
    return min;
  if(val > max)
    return max;
  
  return val;
}

void clearScreen(unsigned char *parlcd_mem_base, unsigned short * fb){
  parlcd_write_cmd(parlcd_mem_base, 0x2c); // write data
  for (int i = 0; i < SCREEN_WIDTH*SCREEN_HEIGHT; i++) {
      fb[i] = 0;
      parlcd_write_data(parlcd_mem_base, fb[i]);
  }
}

void drawColMenu(unsigned short *fb, unsigned short * colors, int colCount, int selected){
  int colMenuWidth = COLOR_MENU_WIDTH;
  int colMenuHeight = SCREEN_HEIGHT / colCount;
  for(int i = 0; i < colCount*colMenuHeight; i++){
    for(int ii = 0; ii < colMenuWidth; ii++){
      int index = i / colMenuHeight;
      if(index == selected && (i % 3 == 0)){
        drawPixelCol(i, SCREEN_WIDTH - ii, 0x00);
        continue;
      }
      drawPixelCol(i, SCREEN_WIDTH - ii, colors[index]);
    }
  }
}

int charWidth(int ch) {
  int width;
  if (!fdes->width) {
    width = fdes->maxwidth;
  } else {
    width = fdes->width[ch-fdes->firstchar];
  }
  return width;
}

void drawChar(int x, int y, char ch, unsigned short color) {
  int w = charWidth(ch);
  const font_bits_t *ptr;
  if ((ch >= fdes->firstchar) && (ch-fdes->firstchar < fdes->size)) {
    if (fdes->offset) {
      ptr = &fdes->bits[fdes->offset[ch-fdes->firstchar]];
    } else {
      int bw = (fdes->maxwidth+15)/16;
      ptr = &fdes->bits[(ch-fdes->firstchar)*bw*fdes->height];
    }
    int i, j;
    for (i=0; i<fdes->height; i++) {
      font_bits_t val = *ptr;
      for (j=0; j<w; j++) {
        if ((val&0x8000)!=0) {
          drawPixelCol(x+j, y+i, color);
        }
        val<<=1;
      }
      ptr++;
    }
  }
}

void drawText(char * text, int len, int x, int y, unsigned short color){
  for(int i = 0; i < len; i++){
    drawChar(x + i*8, y, text[i], color);
  }
}

void drawStats(char* selectedCol, int posX, int posY){
  for(int x = 0; x < 30 * 8; x++){
    for(int y = 0; y < 20; y++){
      drawPixelCol(x, y, 0x00);
    }
  }
  char str[22];
  sprintf(str, "x:%03d y:%03d col:%s", posX, posY, selectedCol);
  drawText(str, 22, 0, 0, 0xFFFF);
}
 
int main(int argc, char *argv[]) {
  unsigned char *mem_base;
  unsigned char *parlcd_mem_base;
  fb  = (unsigned short *)malloc(SCREEN_WIDTH * SCREEN_HEIGHT * sizeof(unsigned short));

  fdes = &font_rom8x16;
 
  sleep(1);
 
  mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  if (mem_base == NULL)
    exit(1);
 
  parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if (parlcd_mem_base == NULL)
    exit(1);

  parlcd_hx8357_init(parlcd_mem_base);
 
  struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 20 * 1000 * 1000};

  unsigned short colors[6] = {0xFFFF, 0xF81F, 0xF800, 0x1F, 0x7E0, 0xFFE0};
  unsigned char colorsLed[6*3] = {
    0xFF, 0xFF, 0xFF, //white
    0xFF, 0x00, 0xFF, //pink
    0x00, 0x00, 0xFF, //red
    0xFF, 0x00, 0x00, //blue
    0x00, 0xFF, 0x00, //green
    0x00, 0xFF, 0xFF  //yellow
  };
  char colorLabels[6][7] = {"white ", "pink  ", "red   ", "blue  ", "green ", "yellow"};

  int colCount = 6;

  int drawPosX = 0;
  int drawPosY = 20;

  int lastRotX = getKnobRotation(mem_base, 0); 
  int lastRotY = getKnobRotation(mem_base, 2);
  
  int selectedCol = 0;

  uint16_t* rgbLine = (uint16_t *)(mem_base + SPILED_REG_LED_RGB2_o);
  *rgbLine = 200;

  drawColMenu(fb, colors, colCount, selectedCol);
  drawToLed(mem_base + SPILED_REG_LED_RGB1_o, colorsLed, selectedCol*3);
  drawToLed(mem_base + SPILED_REG_LED_RGB2_o, colorsLed, selectedCol*3);
  updateScreen(parlcd_mem_base, fb);

  while(1){
    if(getKnobPressed(mem_base, 1)){
      clearScreen(mem_base, fb);
      drawColMenu(fb, colors, colCount, selectedCol);
      updateScreen(parlcd_mem_base, fb);
    }

    if(selectedCol != getKnobRotation(mem_base, 1) / (255 / colCount + 1)){
      selectedCol = getKnobRotation(mem_base, 1) / (255 / colCount + 1);
      drawColMenu(fb, colors, colCount, selectedCol);
      drawToLed(mem_base + SPILED_REG_LED_RGB1_o, colorsLed, selectedCol*3);
      drawToLed(mem_base + SPILED_REG_LED_RGB2_o, colorsLed, selectedCol*3);
    }

    int newRotX = getKnobRotation(mem_base, 0);
    int newRotY = getKnobRotation(mem_base, 2);

    if((newRotX - lastRotX) < -200)
      lastRotX -= 255;
    if((newRotY - lastRotY) < -200)
      lastRotY -= 255;
    if((newRotX - lastRotX) > 200)
      lastRotX += 255;
    if((newRotY - lastRotY) > 200)
      lastRotY += 255;

    int deltaX = newRotX - lastRotX;
    int deltaY = newRotY - lastRotY;

    for(int i = 0; i < abs(deltaX); i++){
      drawPosX += deltaX / abs(deltaX);
      drawPosX = clamp(drawPosX, 0, SCREEN_HEIGHT);
      drawPixelCol(drawPosX, drawPosY, colors[selectedCol]);
    }

    for(int i = 0; i < abs(deltaY); i++){      
      drawPosY += deltaY / abs(deltaY);
      drawPosY = clamp(drawPosY, 20, SCREEN_WIDTH - COLOR_MENU_WIDTH);
      drawPixelCol(drawPosX, drawPosY, colors[selectedCol]);
    }
    
    drawStats(colorLabels[selectedCol], drawPosX, drawPosY);
    updateScreen(parlcd_mem_base, fb);

    lastRotX = newRotX;
    lastRotY = newRotY;
    clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
  }
 
  return 0;
}